﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class entityController : MonoBehaviour {

    public abstract void move(float x, float y);
    public abstract void stopMovement();

}
