﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamestateManager : MonoBehaviour {

    public static GamestateManager self;

    public enum gamestate { playerControl, cutscene };
    public gamestate currentState;

    private void Awake()
    {
        //VERY TEMPORARY - Force 60 FPS
        QualitySettings.vSyncCount = 0;  // VSync must be disabled
        Application.targetFrameRate = 60;

        if (GamestateManager.self == null)
        {
            self = this;
        } else
        {
            Destroy(this.gameObject);
        }
    }

    public void setState(gamestate newState)
    {
        this.currentState = newState;
    }
}
