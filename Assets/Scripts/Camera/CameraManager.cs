﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour {

    public static CameraManager self;

    public GameObject followTarget;
    public float moveSpeed;

    public bool fixedTarget;

    private void Awake()
    {
        if (CameraManager.self == null)
        {
            self = this;
        } else
        {
            Destroy(this.gameObject);
        }
    }

	// Update is called once per frame
	void Update () {
        if (fixedTarget) moveToTargetPos();
        else moveTowardTargetPos();
	}

    public void setTarget(GameObject target)
    {
        this.followTarget = target;
    }

    public void moveToTargetPos()
    {
        transform.position = new Vector3(followTarget.transform.position.x, followTarget.transform.position.y, transform.position.z);
    }

    public void moveTowardTargetPos()
    {
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(followTarget.transform.position.x, followTarget.transform.position.y, transform.position.z), moveSpeed * Time.deltaTime);
    }
    

}
