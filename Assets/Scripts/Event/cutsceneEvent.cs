﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class cutsceneEvent : MonoBehaviour {


    abstract public void act();
    abstract public void afterAct();

}
