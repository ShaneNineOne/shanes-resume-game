﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class cutscene : MonoBehaviour {


    //Trigger conditions
    public bool triggerOnLoad;

    public cutsceneEvent[] events;
    public bool stopControl;
    public bool restoreControl;
    public bool setCutsceneState;

    void startOnLoad()
    {
        if (triggerOnLoad)
        {
            this.fireCutscene();
        }
    }

    private void Start()
    {
        
        if (triggerOnLoad)
        {
            fireCutscene();
        }
    }

    private void fireCutscene()
    {
        cutsceneManager.self.oldCutsceneObj = this.gameObject;
        cutsceneManager.self.events = events;
        cutsceneManager.self.stopControl = stopControl;
        cutsceneManager.self.restoreControl = restoreControl;
        cutsceneManager.self.setCutsceneState = setCutsceneState;
        cutsceneManager.self.launch();
    }
}
