﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waitEvent : cutsceneEvent {

    public float secondsToWait;

    public override void act()
    {
        StartCoroutine(wait());
    }

    public override void afterAct()
    {
        cutsceneManager.self.next();
    }

    public IEnumerator wait()
    {
        yield return new WaitForSeconds(secondsToWait);
        afterAct();
    }
}
