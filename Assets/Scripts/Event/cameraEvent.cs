﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraEvent : cutsceneEvent {

    public GameObject target;
    public bool fixedTarget;
    public bool waitForMovmenetEnd;
    public float moveSpeed;

    public override void act()
    {
        CameraManager.self.setTarget(this.target);
        CameraManager.self.fixedTarget = this.fixedTarget;
        CameraManager.self.moveSpeed = this.moveSpeed;

        if (waitForMovmenetEnd) StartCoroutine(waitForMovementCompletion());
        else afterAct();
        
    }

    public override void afterAct()
    {
        cutsceneManager.self.next();
    }

    public IEnumerator waitForMovementCompletion()
    {
        while (CameraManager.self.transform.position.xy() != target.transform.position.xy())
        {
            yield return null;
        }
        afterAct();
    }
}
