﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogueEvent : cutsceneEvent {

    public string[] dialogue;
    public bool waitForDialogueEnd;

    public override void act()
    {
        DialogueManager.self.dialogueLines = dialogue;
        DialogueManager.self.showDialogue();
        if (waitForDialogueEnd)
        {
            StartCoroutine(waitForDialogueCompletion());
        } 
    }


    public override void afterAct()
    {
        cutsceneManager.self.next();
    }

    public IEnumerator waitForDialogueCompletion()
    {
        while (DialogueManager.self.isActive)
        {
            yield return null;
        }
        afterAct();
    }
}
