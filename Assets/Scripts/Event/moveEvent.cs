﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveEvent : cutsceneEvent {

    public entityController moveObj;
    public GameObject target;
    public Vector2 diff;

    /// <summary>
    /// Should the entity move along the vertical axis first? Will move along horizontal axis first if not.
    /// </summary>
    public bool verticalFirst;

    /// <summary>
    /// Should the entity only move along the horizontal axis?
    /// </summary>
    public bool horizontalOnly;
    /// <summary>
    /// Should the entity only move along the vertical axis?
    /// </summary>
    public bool verticalOnly;

    public bool posXReached;
    public bool posYReached;

    //TODO: Make this variable part of the abstract class
    public bool waitForEnd;

    public override void act()
    {
        diff = moveObj.transform.position - target.transform.position;

        if (verticalOnly || verticalFirst) StartCoroutine(this.verticalMoveToPos());
        else StartCoroutine(this.horizontalMoveToPos());

        if (!this.waitForEnd) {
            afterAct();
        }
    }

    public override void afterAct()
    {
        cutsceneManager.self.next();
    }

    private IEnumerator horizontalMoveToPos()
    {
        bool leftApproach = diff.x < 0;
        while (!posXReached)
        {
            if (leftApproach)
            {
                moveObj.move(1, 0);
                posXReached = moveObj.transform.position.x > target.transform.position.x;
            }
            else
            {
                moveObj.move(-1, 0);
                posXReached = moveObj.transform.position.x < target.transform.position.x;
            }

            yield return null;
        }

        if (!posYReached && !horizontalOnly) StartCoroutine(verticalMoveToPos());
        else
        {
            moveObj.stopMovement();         //TODO: UGLY. UGLY. Move this into the entityController itself somehow!
            if (waitForEnd) afterAct();
        }
    }

    private IEnumerator verticalMoveToPos()
    {
        bool bottomApproach = diff.y < 0;
        while (!posYReached)
        {
            if (bottomApproach)
            {
                moveObj.move(0, 1);
                posYReached = moveObj.transform.position.y > target.transform.position.y;
            } else
            {
                moveObj.move(0, -1);
                posYReached = moveObj.transform.position.y < target.transform.position.y;
            }
           
            yield return null;
        }

        if (!posXReached && !verticalOnly) StartCoroutine(horizontalMoveToPos());
        else
        {
            moveObj.stopMovement();         //TODO: UGLY. UGLY. Move this into the entityController itself somehow!
            if (waitForEnd) afterAct();
        }
    }
    
}
