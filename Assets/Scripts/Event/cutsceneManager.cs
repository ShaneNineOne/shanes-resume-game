﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class cutsceneManager : MonoBehaviour {

    public static cutsceneManager self;

    public bool stopControl;
    public bool restoreControl;
    public bool setCutsceneState;

    public GameObject oldCutsceneObj;

    private GameObject oldCamTarget;
    private float oldCamSpeed;
    private bool oldCamFixedTarget;

    public cutsceneEvent[] events;

    [SerializeField] private int eventIndex;

    private void Awake()
    {
        if (cutsceneManager.self == null)
        {
            self = this;
        } else
        {
            Destroy(this.gameObject);
        }
    }

    public void launch()
    {
        PlayerState.self.canMove = !stopControl;
        oldCamSpeed = CameraManager.self.moveSpeed;
        oldCamTarget = CameraManager.self.followTarget;
        oldCamFixedTarget = CameraManager.self.fixedTarget;
        StartCoroutine(act());
    }

    public void next()
    {
        eventIndex++;
        if (eventIndex >= events.Length)
        {
            Destroy(oldCutsceneObj);
            CameraManager.self.followTarget = oldCamTarget;
            CameraManager.self.moveSpeed = oldCamSpeed;
            CameraManager.self.fixedTarget = oldCamFixedTarget;
            PlayerState.self.canMove = restoreControl;
            eventIndex = 0;
        } else
        {
            StartCoroutine(act());
        }
    }

    public IEnumerator act()
    {
        events[eventIndex].act();
        yield return null;
    }
    
}
