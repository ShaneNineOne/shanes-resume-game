﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testEvent : cutsceneEvent {

    public string test;

    public override void act()
    {
        Debug.Log(test);
        afterAct();
    }

    public override void afterAct()
    {
        cutsceneManager.self.next();
    }
}
