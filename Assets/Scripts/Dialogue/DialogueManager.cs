﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    public static DialogueManager self;

    public GameObject dBox;
    public Text dText;

    public bool isActive;

    public string[] dialogueLines;

    private int currentLine;
    private bool stopControl;
    private bool restoreControlAfter;

    private void Awake()
    {
        if (DialogueManager.self == null)
        {
            self = this;
        } else
        {
            Destroy(this.gameObject);
        }
        
    }
    
	// Update is called once per frame
	void Update () {
	    if (isActive && Input.GetKeyDown(KeyCode.Space))
        {
            currentLine++;

            if (currentLine >= dialogueLines.Length)
            {
                dBox.SetActive(false);
                isActive = false;
                currentLine = 0;

                if (restoreControlAfter)
                {
                    PlayerState.self.canMove = true;
                }
            }

            dText.text = dialogueLines[currentLine];
        }	
	}

    public void showDialogue()
    {
        isActive = true;
        dText.text = dialogueLines[0];
        dBox.SetActive(true);
        if (stopControl)
        {
            PlayerState.self.canMove = false;
            PlayerController.self.stopMovement();
        }
    }
    
    public void setText(string[] dialogue, bool controlBefore, bool controlAfter)
    {
        this.dialogueLines = dialogue;
        this.stopControl = controlBefore;
        this.restoreControlAfter = controlAfter;
    }
    
    public void setText(string[] dialogue)
    {
        this.dialogueLines = dialogue;
        this.stopControl = false;
        this.restoreControlAfter = false;
    }
}
