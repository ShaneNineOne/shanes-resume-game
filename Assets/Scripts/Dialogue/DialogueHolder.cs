﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueHolder : MonoBehaviour {

    [SerializeField] private string[] dialogue;
    //Throw these into a higher level "Event Controller" as potential events later.
    //All dialogue sequences will then be events, in part.
    [SerializeField] private bool stopControl;
    [SerializeField] private bool restoreControl;

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.CompareTag("Player") && !DialogueManager.self.isActive && Input.GetKeyUp(KeyCode.Space))
        {
            DialogueManager.self.setText(dialogue, stopControl, restoreControl);
            DialogueManager.self.showDialogue();
        }
    }
}
