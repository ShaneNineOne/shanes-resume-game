﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerState : MonoBehaviour {

    public static PlayerState self;

    private void Awake()
    {
         if (PlayerState.self == null)
        {
            self = this;
        } else
        {
            Destroy(this.gameObject);
        }
    }

    public bool canMove;
}
