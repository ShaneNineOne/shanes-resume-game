﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : entityController {

    public static PlayerController self;
    [SerializeField] private float maxSpeed;

    private Rigidbody2D rigid;
    private Animator anim;
    private bool isMoving;
    private Vector2 lastMove;


    private void Awake()
    {
        if (PlayerController.self == null)
        {
            self = this;
        } else
        {
            Destroy(this.gameObject);
        }

        rigid = this.gameObject.GetComponent<Rigidbody2D>();
        anim = this.gameObject.GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        //TODO: Better controls for movement... we're stopping all movement right now outside of the player controller if there's no input from any source. Detect that here?
        if (PlayerState.self.canMove)
        {
            this.move(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        }
    }

    public override void move(float xMovement, float yMovement)
    {
        isMoving = Mathf.Abs(xMovement) > 0.5f || Mathf.Abs(yMovement) > 0.5f;
        Vector2 movement = new Vector2(xMovement * maxSpeed * Time.deltaTime, yMovement * maxSpeed * Time.deltaTime);
        anim.SetBool("isMoving", isMoving);

        if (isMoving)
        {
            lastMove = movement;
            rigid.velocity = lastMove;
            anim.SetFloat("lastMoveX", xMovement);
            anim.SetFloat("lastMoveY", yMovement);
        }

        rigid.velocity = movement;
        anim.SetFloat("moveX", xMovement);
        anim.SetFloat("moveY", yMovement);
    }

    public override void stopMovement()
    {
        rigid.velocity = Vector3.zero;
        anim.SetFloat("moveX", 0);
        anim.SetFloat("moveY", 0);
        anim.SetBool("isMoving", false);
    }
}
